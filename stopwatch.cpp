/*
 *  Project:  motion balancer
 * 
 *  Date:     14.05.2019
 *  File:     stopwatch.cpp
 *
 *  Author:   rth.
 *
 *  License:  General Public License, Version 3 or newer
 *            http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

#include <Arduino.h>

#include "stopwatch.h"


static long times[TIMES];      // array stores all time values
static int nextTimeVal = 0;    // array pointer to the oldest time to be replaced
static int toggleOutputPin;
/*
 *
 */
void initStopWatch(OUT toggle) {

  toggleOutputPin = toggle;
  
  for(int t=0; t<TIMES; t++) {
    times[t] = 0;
  }
  
}



/*
 *
 */
void addTime2StopWatch(void) {

  static bool toggle = false;
  
  digitalWrite(toggleOutputPin, toggle);
  toggle = !toggle;

  times[nextTimeVal] = millis();
  nextTimeVal++;
  if(nextTimeVal>=TIMES) nextTimeVal = 0;
}



/*
 *
 */
long getAverageTime(void) {
  long average = 0;    // average start value
  
  // set the read pointer to the last entry
  int readPointer;
  if(nextTimeVal == 0) readPointer = TIMES-1;
  else       readPointer = nextTimeVal - 1;
  
  for(int t=1; t<TIMES; t++) {  // for all time values in array
    // calculate and add the differences of the time entries to average
    if(readPointer == 0) {
      average += times[readPointer] - times[TIMES-1];
    } else {  
      average += times[readPointer] - times[readPointer-1];
    }
    readPointer--;  // point to the next older time value
    if(readPointer < 0) readPointer = TIMES - 1; // set the pointer back to the end
  }
  return average/TIMES; // return the average
}
