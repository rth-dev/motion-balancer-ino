/*
 *  Project:  motion balancer
 * 
 *  Date:     14.05.2019
 *  File:     display.h
 *
 *  Author:   rth.
 *
 *  License:  General Public License, Version 3 or newer
 *            http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

#ifndef DISPLAY_H
#define DISPLAY_H

#include "control.h"

#define PIN_DISPLAY_RESET     PIN_LED_RED  // Display Reset Pin
#define SCREEN_WIDTH          128          // OLED display width, in pixels
#define SCREEN_HEIGHT         64           // OLED display height, in pixels

void initDisplay(void);

void clearDisplay(void);
void updateDisplay(long cycle_time);


#endif /* DISPLAY_H */
