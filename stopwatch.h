/*
 *  Project:  motion balancer
 * 
 *  Date:     14.05.2019
 *  File:     stopwatch.h
 *
 *  Author:   rth.
 *
 *  License:  General Public License, Version 3 or newer
 *            http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

#ifndef STOPWATCH_H
#define STOPWATCH_H

#include "control.h"

#define TIMES 20    // amount of time values on which the average is calculated


void initStopWatch(OUT toggle);   // initilizes the stop watch 
void addTime2StopWatch(void);  // adds a new time 
long getAverageTime(void);     // returns the average



#endif /* STOPWATCH_H */
