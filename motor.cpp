/*
 *  Project:  motion balancer
 * 
 *  Date:     14.05.2019
 *  File:     motor.cpp
 *
 *  Author:   rth.
 *
 *  License:  General Public License, Version 3 or newer
 *            http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

#include <Arduino.h>

#include "motor.h"
#include "stddef.h"


static TwoWire *wire;
static MOTOR_T mLeft;
static MOTOR_T mRight;


/* 
 * 
 */
void activateMotorSpeed() {
  wire->beginTransmission(MOTORI2CADDR); // transmit to device I2CAddr
  wire->write(MOTORI2CSPEED);       // set pwm header 
  wire->write(mLeft.speed);         // send pwma 
  wire->write(mRight.speed);        // send pwmb    
  wire->endTransmission();          // stop transmitting
//  delay(5);  // necessary ??
}


/* 
 * 
 */
void activateMotorDirection() {
  unsigned char dir = (mLeft.direction << 2);
  dir |= mRight.direction;
  wire->beginTransmission(MOTORI2CADDR);  // transmit to device I2CAddr
  wire->write(MOTORI2CDIRECTION);         // Direction control header
  wire->write(dir);                       // send direction control information
  wire->write(MOTORNOP);                  // need to send this byte as the third byte(no meaning)  
  wire->endTransmission();                // stop transmitting 
//  delay(5);  // necessary ??
}


/* 
 * 
 * 
 */
void initMotor(TwoWire* w) {
  wire = w;
  mLeft.speed = 0;
  mLeft.direction = CW;
  mRight.speed = 0;
  mRight.direction = CW;
  activateMotorSpeed();
  delay(10);  // necessary ??
  activateMotorDirection();
  delay(10);  // necessary ??
  setMotorSpeed(BOTH, 0);
  delay(10);  // necessary ??  
  setMotorDirection(BOTH, CW);
  delay(10);  // necessary ??
}


/* 
 * 
 */
void setMotorSpeed(MOTOR m, unsigned char sp) {
  if(m == LEFT)  mLeft.speed = sp;
  if(m == RIGHT) mRight.speed = sp;
  if(m == BOTH)  {mLeft.speed = sp; mRight.speed = sp;}
  activateMotorSpeed();
}


/* 
 * //"0b1010" defines the output polarity, "10" means the M+ is "positive" while the M- is "negtive"
 */
void setMotorDirection(MOTOR m, DIRECTION d) {
  if(m == LEFT ) mLeft.direction  = d; 
  if(m == RIGHT) mRight.direction = d;
  if(m == BOTH)  {mLeft.direction = d, mRight.direction = d;}
  activateMotorDirection();
}


/*
 *
 */
int getMotorSpeed(MOTOR m) {
  if(m == LEFT)  return mLeft.speed;
  if(m == RIGHT) return mRight.speed;
  return ERR;
}


/*
 *
 */
int getMotorDirection(MOTOR m) {
  if(m == LEFT)  return mLeft.direction;
  if(m == RIGHT) return mLeft.direction;
  return ERR;
}
