/*
 *  Project:  motion balancer
 * 
 *  Date:     14.05.2019
 *  File:     motion-balancer.ino
 *
 *  Author:   rth.
 *
 *  License:  General Public License, Version 3 or newer
 *            http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>



// ***  3rd party - inlcudes here because Arduino IDE doesn't find them  ***
//         libraries needs to be placed in "../sketchbook/libraries/"
// #include <Adafruit_GFX.h>      // Adafruit GFX library  https://github.com/adafruit/Adafruit-GFX-Library
// #include <Adafruit_SSD1306.h>  // Adafruit OLED driver  https://github.com/adafruit/Adafruit_SSD1306 
// #include <LSM303.h>            // AltIMU-10 v3 acc+mag  https://github.com/pololu/lsm303-arduino    
// #include <L3G.h>               // AltIMU-10 v3 gyro     https://github.com/pololu/l3g-arduino
// #include <LPS.h>               // AltIMU-10 v3 barom    https://github.com/pololu/lps-arduino


#include "stddef.h"
#include "control.h"
#include "motor.h"
#include "display.h"
#include "sensor.h"
#include "stopwatch.h"


// task scheduler
#define DISPLAY_TASK_HZ   10    // display task in Hz
#define PANEL_CHANGE_SEC   5    // change OLED panel after x seconds
float dt = 1.0;                 // real duration of the main run
long  timer = 0;
long  timer_old;
unsigned int pannelCountmsec = 0;



/* 
 * 
 * 
 */
void setup()  {

  Serial1.begin(BAUDRATE);
  Wire.begin();
  timer = millis();


  initControl(MANUAL, FADE);  // MODE={MANUAL|INTERN|REMOTE}  LEDMODE={ONOFF|FADE}
  enableLED(LED_INIT);

  initDisplay();  
  initSensor();
  initMotor(&Wire);
  initStopWatch(TOGGLE);
  

  disableLED(LED_INIT);
}




/* 
 * 
 * 
 */
void loop()  {


  ///// SWITCH MODES /////////////////////////////////////////
  if(  getSwitchState(SW_AM) &&  getSwitchState(SW_IE) ) setMode(REMOTE);
  if( !getSwitchState(SW_AM) &&  getSwitchState(SW_IE) ) setMode(REMOTE);
  if(  getSwitchState(SW_AM) && !getSwitchState(SW_IE) ) setMode(INTERN);
  if( !getSwitchState(SW_AM) && !getSwitchState(SW_IE) ) setMode(MANUAL);


  ///// DISPLAY TASK /////////////////////////////////////////
  if ((millis() - timer) >= (1000/DISPLAY_TASK_HZ)) {
    timer_old = timer;
    timer = millis();
    if (timer > timer_old)
      dt = (timer - timer_old) / 1000.0; // Real time of loop run
    else
      dt = 0;
    // check whether display panel needs to be switched
    pannelCountmsec += 1000/DISPLAY_TASK_HZ;
    if(pannelCountmsec/1000 > PANEL_CHANGE_SEC) {
      pannelCountmsec = 0;
    }
    INFIF.print("update ");
    INFIF.print(timer);
    INFIF.print("\n\r");
    updateDisplay(getAverageTime());
  } // end display task



  ///// CONTROL TASK - MANUAL ///////////////////////////////////////
  if(getMode() == MANUAL) { // MODE=MANUAL
    int speedVal = getPotiVal(POTI_SPD);
    // read buttons
    int bLeft = getButtonState(SW_LEFT);
    int bRight = getButtonState(SW_RIGHT);
    // set motors & LEDs
    if(bLeft == HIGH && bRight == LOW) {
      fadeLED(LED_LEFT, speedVal);
      fadeLED(LED_RIGHT, 0);
      setMotorSpeed(LEFT, speedVal);
      setMotorSpeed(RIGHT, 0);
    } else if(bLeft == LOW && bRight == HIGH) {
      fadeLED(LED_LEFT, 0);
      fadeLED(LED_RIGHT, speedVal);
      setMotorSpeed(LEFT, 0);
      setMotorSpeed(RIGHT, speedVal);
    } else if(bLeft == LOW && bRight == LOW) {
      fadeLED(LED_LEFT, 0);
      fadeLED(LED_RIGHT, 0);
      setMotorSpeed(BOTH, 0);
    } else if(bLeft == HIGH && bRight == HIGH) {
      fadeLED(LED_LEFT, speedVal);
      fadeLED(LED_RIGHT, speedVal);
      setMotorSpeed(LEFT, speedVal);
      setMotorSpeed(RIGHT, speedVal);
    }

    #if DBGMSG
      DBGIF.print("DBG> MANUAL msec=");
      DBGIF.println(millis());
    #endif
  } // end-of MODE=MANUAL
   
   
   
   
  ///// CONTROL TASK - INTERN ///////////////////////////////////////
  if(getMode() == INTERN) { // MODE=INTERN
    int sl=0, sr=0;
    if(getDegree() > 0) {
      setMotorSpeed(LEFT, 0);
      sl = map(getDegree() * getPotiVal(POTI_P), 0, 23000, 0, 255);
      setMotorSpeed(RIGHT, sl);
    }
    if(getDegree() < -0) {
      sr = map(getDegree() * getPotiVal(POTI_P) * -1, 0, 23000, 0, 255); 
      setMotorSpeed(LEFT, sr);
      setMotorSpeed(RIGHT, 0);
    }
    fadeLED(LED_LEFT, getMotorSpeed(LEFT));
    fadeLED(LED_RIGHT, getMotorSpeed(RIGHT));

    #if DBGMSG
      DBGIF.print("DBG> INTERN msec=");
      DBGIF.print(millis());
      DBGIF.print("  poti=");
      DBGIF.print(getPotiVal(POTI_P));
      DBGIF.print("  sl=");
      DBGIF.print(sl);
      DBGIF.print("  sr=");
      DBGIF.println(sr);
    #endif
    } // end-of MODE=INTERN /////////////////////////////////////////
   


   
  ///// CONTROL TASK - REMOTE ///////////////////////////////////////
  if(getMode() == REMOTE) { // MODE=REMOTE

    // TODO;

    #if DBGMSG
      DBGIF.print("DBG> REMOTE msec=");
      DBGIF.println(millis());
    #endif
  } // end-of MODE=REMOTE ///////////////////////////////////////////
   
   
  addTime2StopWatch(); // add new time StopWatch
} // end loop()
