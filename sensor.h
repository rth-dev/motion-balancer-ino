/*
 *  Project:  motion balancer
 * 
 *  Date:     14.05.2019
 *  File:     sensor.h
 *
 *  Author:   rth.
 *
 *  License:  General Public License, Version 3 or newer
 *            http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

#ifndef SENSOR_H
#define SENSOR_H


void initSensor(void);     // initialize the sensor

int getCompassAccX(void);  // returns compass x accelerometer value
int getCompassAccY(void);  // returns compass y accelerometer value
int getCompassAccZ(void);  // returns compass z accelerometer value
int getCompassMagX(void);  // returns compass x magnetometer value
int getCompassMagY(void);  // returns compass y magnetometer value
int getCompassMagZ(void);  // returns compass z magnetometer value
int getDegree(void);       // returns degree value of x

int getGyroX(void);  // returns gyro x value
int getGyroY(void);  // returns gyro y value
int getGyroZ(void);  // returns gyro z value

float getPressure(void);      // returns pressure
float getAltitude(void);      // returns alitude
float getTemperatureC(void);  // returns temperature


#endif /* SENSOR_H */
