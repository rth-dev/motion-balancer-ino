/*
 *  Project:  motion balancer
 * 
 *  Date:     14.05.2019
 *  File:     control.h
 *
 *  Author:   rth.
 *
 *  License:  General Public License, Version 3 or newer
 *            http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

#ifndef CONTROL_H
#define CONTROL_H


#define PIN_TOG_OUT      12    // Output toggles in main loop

#define PIN_SDA           2    // TWI data SDA
#define PIN_SCL           3    // TWI clock SCL
#define PIN_LED_RED       5    // LED 1 - green
#define PIN_LED_GREEN    13    // LED 2 - red
#define PIN_LED_LEFT     11    // LED left
#define PIN_LED_RIGHT    10    // LED right

#define PIN_SW_LEFT      9    // left switch
#define PIN_SW_RIGHT     8    // reight switch
#define PIN_SW_AM        7    // switch auto/manual
#define PIN_SW_IE        6    // switch intern/extern

#define PIN_POTI_SPD    A4    // Poti 1  - speed
#define PIN_POTI_P      A5    // Poti 2  - 'P' paramerter
#define PIN_POTI_I      A6    // Poti 3  - 'I' paramerter
#define PIN_POTI_D      A11   // Poti 4  - 'D' paramerter


enum MODE    { MANUAL, INTERN, REMOTE };  // TODO: implementation of INTERNAL and REMOTE control loop
enum BUTTON  { SW_LEFT=PIN_SW_LEFT, SW_RIGHT=PIN_SW_RIGHT };
enum SWITCH  { SW_AM=PIN_SW_AM, SW_IE=PIN_SW_IE };
enum POTI    { POTI_SPD=PIN_POTI_SPD, POTI_P=PIN_POTI_P, POTI_I=PIN_POTI_I, POTI_D=PIN_POTI_D };
enum LED     { LED_INIT=PIN_LED_GREEN, LED_LEFT=PIN_LED_LEFT, LED_RIGHT=PIN_LED_RIGHT };
enum OUT     { TOGGLE=PIN_TOG_OUT };
enum LEDMODE { ONOFF, FADE };

int  initControl(MODE m, LEDMODE lm);      // initializes the controls

void setMode(MODE s);            // sets the current mode
MODE getMode(void);              // returns the current mode
bool getButtonState(BUTTON b);   // returns the current state of a button
bool getSwitchState(SWITCH s);   // returns the current state of a switch
int  getPotiVal(POTI p);         // returns potentiometer value 

void  fadeLED(LED led, int val);   //  fades a LED according to the given value
void  enableLED(LED led);          //  LED switch on (brightest level)
void  disableLED(LED led);         //  LED switch off


#endif /* CONTROL_H */
