/*
 *  Project:  motion balancer
 * 
 *  Date:     14.05.2019
 *  File:     motor.h
 *
 *  Author:   rth.
 *
 *  License:  General Public License, Version 3 or newer
 *            http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

#ifndef MOTOR_H
#define MOTOR_H

#include <Wire.h> 


#define MOTORI2CADDR       0x0F    // Set the address of the I2CMotorDriver
#define MOTORI2CSPEED      0x82    // cmd byte motor speed
#define MOTORI2CDIRECTION  0xAA    // cmd byte motor direction
#define MOTORI2CLEFT       0xA1    // cmd byte select motor left
#define MOTORI2CRIGHT      0xA5    // cmd byte select motor right
#define MOTORNOP           0x01    // nop cmd


enum MOTOR  {LEFT=MOTORI2CLEFT, RIGHT=MOTORI2CRIGHT, BOTH};  // 
enum DIRECTION {CW=0b10, CCW=0b01};  // CW: CLOCKWISE CCW: COUNTERCLOCK

typedef struct MOTOR_S {
  unsigned char speed;      // 0 - 255
  unsigned char direction;  // CW: 0b10 CCW: 0b01
} MOTOR_T;


void initMotor(TwoWire* w);

void setMotorSpeed(MOTOR m, unsigned char speed);
void setMotorDirection(MOTOR m, DIRECTION d);
int  getMotorSpeed(MOTOR m);
int  getMotorDirection(MOTOR m);


#endif /* MOTOR_H */
