/*
 *  Project:  motion balancer
 * 
 *  Date:     14.05.2019
 *  File:     stddef.h
 *
 *  Author:   rth.
 *
 *  License:  General Public License, Version 3 or newer
 *            http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

#ifndef STDDEF_H
#define STDDEF_H


#define PI       3.14159

#define OK       0
#define ERR     -1

#define INFMSG   1
#define DBGMSG   0
#define DBGIF    Serial1
#define INFIF    Serial1

#define BAUDRATE 115200


#endif /* STDDEF_H */
