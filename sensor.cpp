/*
 *  Project:  motion balancer
 * 
 *  Date:     14.05.2019
 *  File:     sensor.cpp
 *
 *  Author:   rth.
 *
 *  License:  General Public License, Version 3 or newer
 *            http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

#include <Arduino.h>
#include <LSM303.h>  // AltIMU-10 v3 acc+mag  https://github.com/pololu/lsm303-arduino    
#include <L3G.h>     // AltIMU-10 v3 gyro     https://github.com/pololu/l3g-arduino
#include <LPS.h>     // AltIMU-10 v3 barom    https://github.com/pololu/lps-arduino

#include "sensor.h"
#include "stddef.h"

static LSM303 compass;  // LSM303D 3-axis accelerometer and 3-axis magnetometer
static L3G gyro;        // L3GD20H 3-axis gyroscope
static LPS ps;          // LPS331AP digital barometer



/* 
 * 
 */
void initSensor() {

  if (!compass.init()) {
    if(DBGMSG) DBGIF.println("DBG> initSensor(): err comp");
  } else {
    compass.enableDefault();
    compass.read();
  }

  if (!gyro.init()) {
    if(DBGMSG) DBGIF.println("DBG> initSensor(): err gyro");
  } else {
    gyro.enableDefault();
    gyro.read();
  }
      
  if (!ps.init()) {
    if(DBGMSG) DBGIF.println("DBG> initSensor(): err baro");
  } else { 
    ps.enableDefault();
  }
}


/* 
 * 
 */
int getCompassAccX(void) {
  compass.read();
  return compass.a.x;
}


/* 
 * 
 */
int getCompassAccY(void) {
  compass.read();
  return compass.a.y;
}


/* 
 * 
 */
int getCompassAccZ(void) {
  compass.read();
  return compass.a.z;
}


/* 
 * 
 */
int getCompassMagX(void) {
  compass.read();
  return compass.m.x;
}


/* 
 * 
 */
int getCompassMagY(void) {
  compass.read();
  return compass.m.y;
}


/* 
 * 
 */
int getCompassMagZ(void) {
  compass.read();
  return compass.m.z;
}


/* 
 * 
 */
int getDegree(void) {
  return map(getCompassAccX(), -16384, +16383,-90, +90);  
}


/* 
 * 
 */
int getGyroX(void) {
  gyro.read();
  return gyro.g.x;
}


/* 
 * 
 */
int getGyroY(void) {
  gyro.read();
  return gyro.g.y;
}


/* 
 * 
 */
int getGyroZ(void) {
  gyro.read();
  return gyro.g.z;
}


/* 
 * 
 */
float getPressure(void) {
  return ps.readPressureMillibars();
}


/* 
 * 
 */
float getAltitude(void) {
  return ps.pressureToAltitudeMeters(getPressure());
}


/* 
 * 
 */
float getTemperatureC(void) {
  return ps.readTemperatureC();
}
