/*
 *  Project:  motion balancer
 * 
 *  Date:     14.05.2019
 *  File:     display.cpp
 *
 *  Author:   rth.
 *
 *  License:  General Public License, Version 3 or newer
 *            http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

#include <Arduino.h>
#include <string.h>
#include <avr/pgmspace.h>

#include <Adafruit_GFX.h>      // Adafruit GFX library  https://github.com/adafruit/Adafruit-GFX-Library
#include <Adafruit_SSD1306.h>  // Adafruit OLED driver  https://github.com/adafruit/Adafruit_SSD1306 



#include "display.h"
#include "motor.h"
#include "sensor.h"
#include "stddef.h"


#define BIG_TEXT_SIZE         2    // big text size
#define BIG_LINES             4    // total lines for big text
#define BIG_LINE_WIDTH       11    // charactors per line (10+1 => 10+'\n' )
#define SMALL_TEXT_SIZE       1    // small text size
#define SMALL_LINES           8    // total lines for big text 
#define SMALL_LINE_WIDTH     22    // charactors per line (21+1 => 21+'\n' )


Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, PIN_DISPLAY_RESET);    // the display


const char lineModes[][7] PROGMEM = {
          //                          123456
          /*   0 manual         */   "manual",
          /*   1 intern         */   "intern",
          /*   2 remote         */   "remote"};


/* 
 * 
 */
void drawAnimation() {

  int len=48, cw, ch, wPx, hPx;
  cw = display.width()/2;  // get center of display
  ch = display.height()/2; //
  float deg = getDegree();
  if(deg > -8 && deg < 8 ) len = 62;
  wPx = len * cos(deg * (PI/180));  // offset width
  hPx = len * sin(deg * (PI/180));  // offset height
  
  // draw center point and SMALL_LINES
  display.fillCircle(cw, ch, 2, WHITE);
  display.drawLine(cw-wPx, ch-hPx, cw+wPx, ch+hPx, WHITE);
}



/* 
 * 
 */
void drawText(long cycle_time) {

  // MODES
  display.setCursor(0,0);
  if(getMode() == MANUAL) {
    display.print("M");
  }
  else if(getMode() == INTERN) {
    display.print("I");
  }
  else if(getMode() == REMOTE) {
    display.print("R");
  }
  
  // temporarily line
  char value[5];

  // CYCLE TIME
  display.setCursor(98,0);
  snprintf(value, 6, "%5i", cycle_time);
  display.print(value);

  // DEGREE
  display.setCursor(0,56);
  snprintf(value, 6, "%3i", getDegree());
  display.print(value);
  
  // MOTOR SPEED
  display.setCursor(98,47);
  snprintf(value, 6, "%5i", getMotorSpeed(LEFT));
  display.print(value);
  display.setCursor(98,56);
  snprintf(value, 6, "%5i", getMotorSpeed(RIGHT));
  display.print(value);

}


/* 
 * 
 */
void updateDisplay(long cycle_time) {

  // update OLDED
  display.clearDisplay();
  display.setCursor(0,0);

  drawAnimation();
  drawText(cycle_time);

  // show display
  display.display(); 
}



/* 
 * 
 */
void clearDisplay(void) {
  display.clearDisplay();
  display.display(); 
}



/* 
 * 
 */
void initDisplay(void) {
  // OLED dispaly setup
  pinMode(PIN_DISPLAY_RESET, OUTPUT);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3D);  // initialize with the I2C addr 0x3D (for the 128x64)
  display.setTextSize(SMALL_TEXT_SIZE);
  display.setTextColor(WHITE);
  clearDisplay();
}
