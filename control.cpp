/*
 *  Project:  motion balancer
 * 
 *  Date:     14.05.2019
 *  File:     control.cpp
 *
 *  Author:   rth.
 *
 *  License:  General Public License, Version 3 or newer
 *            http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

#include <Arduino.h>

#include "control.h"
#include "stddef.h"

MODE currentMode;
LEDMODE ledmode;

/* 
 * 
 * 
 */
int initControl(MODE m, LEDMODE lm) {

  currentMode = m;
  ledmode = lm;

  pinMode(PIN_SW_LEFT, INPUT);
  pinMode(PIN_SW_RIGHT, INPUT);
  pinMode(PIN_SW_AM, INPUT);
  pinMode(PIN_SW_IE, INPUT);
  pinMode(PIN_POTI_SPD, INPUT);
  pinMode(PIN_POTI_P, INPUT);
  pinMode(PIN_POTI_I, INPUT);
  pinMode(PIN_POTI_D, INPUT);
  pinMode(LED_INIT, OUTPUT);
  pinMode(TOGGLE, OUTPUT);

  if(lm != FADE) {
    pinMode(PIN_LED_LEFT, OUTPUT);
    pinMode(PIN_LED_RIGHT, OUTPUT);
  }
  return OK;
}


/* 
 * 
 * 
 */
MODE getMode(void) {
  return currentMode;
}


/* 
 * 
 * 
 */
void setMode(MODE m) {
  currentMode = m;
}


/* 
 * 
 * 
 */
bool getButtonState(BUTTON b) {
  return digitalRead(b);
}


/* 
 * 
 * 
 */
bool getSwitchState(SWITCH s) {
  return digitalRead(s);
}


/* 
 * 
 * 
 */
int getPotiVal(POTI p) {
  return map(analogRead(p), 0, 1023, 0, 255);
}


/* 
 * 
 * 
 */
void fadeLED(LED led, int val) {
  if(ledmode == FADE) {  
    analogWrite(led, val);
  } else {
    if(val > 0) enableLED(led);
    else disableLED(led);

  }
}


/* 
 * 
 * 
 */
void enableLED(LED led) {
  if(ledmode != FADE) {  
    digitalWrite(led, HIGH);
  } else {
    fadeLED(led, 255);
  }
}


/* 
 * 
 * 
 */
void disableLED(LED led) {
  if(ledmode != FADE) {  
    digitalWrite(led, LOW);
  } else {
    fadeLED(led, 0);
  }
}
